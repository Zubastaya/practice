package ru.zubastaya.practice;

import java.util.Date;

public class Person {

    private String name;
    private double height;
    private double weight;
    private Date birthday;

    public Person() {
        name = "";
        birthday = null;
    }

    public Person(String name, double height, double weight, Date birthday) {
        this.name = name;
        this.height = height;
        this.weight = weight;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString(){
        return name+" Рост-"+height+" Вес-"+weight+" Дата рождения-"+birthday;
    }
}
